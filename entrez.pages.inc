<?php
/**
 * @file
 * Forms for the entrez module.
 */

/**
 * Form to import from Entrez using search terms.
 *
 * @return
 *   An array which will be used by the form builder to build the import form.
 * @ingroup forms
 * @see entrez_search_import_form_validate()
 * @see entrez_search_import_form_submit()
 */
function entrez_search_import_form() {
  $esearch_help = 'http://eutils.ncbi.nlm.nih.gov/corehtml/query/static/esearch_help.html';
  $form['entrez_pubmed_search'] = array(
    '#type' => 'fieldset',
    '#title' => t('PubMed search'),
    '#description' => t('Define your PubMed search.'),
    'search_query' => array(
      '#type' => 'textarea',
      '#title' => t('Search terms'),
      '#description' => '<p>' . l('Search terms', $esearch_help . '#Searchterms') . '</p>',
      '#default_value' => entrez_get_search_query(),
      '#required' => FALSE,
    ),
    'min_date' => array(
      '#type' => 'date_select',
      '#title' => t('Minimum date'),
      '#description' => '<p>' . l('Minimum date.', $esearch_help . '#DateRanges') . '</p>',
      '#default_value' => entrez_get_min_date('current'),
      '#date_year_range' => '-30:+0',
      '#required' => TRUE,
    ),
    'max_date' => array(
      '#type' => 'date_select',
      '#title' => t('Maximum date'),
      '#description' => '<p>' . l('Maximum date.', $esearch_help . '#DateRanges') . '</p>',
      '#default_value' => entrez_get_max_date('current'),
      '#date_year_range' => '-30:+0',
      '#required' => TRUE,
    ),
    'button_batch_import' => array(
      '#type' => 'submit',
      '#value' => t('Import from PubMed'),
    ),
  );

  return $form;
}

/**
 * Returns a form used to import PubMed into biblio by PMID.
 *
 * @return
 *   An array which will be used by the form builder to build the import form.
 * @ingroup forms
 * @see entrez_pubmed_id_import_form_validate()
 * @see entrez_pubmed_id_import_form_submit()
 */
function entrez_pubmed_id_import_form() {
  global $user;

  $form['entrez_pubmed_id'] = array(
    '#type' => 'textfield',
    '#title' => t('PubMed ID'),
    '#description' => t('One or more PubMed unique identifiers of articles.  If more than one, separate by spaces, commas, or semicolons.'),
    '#required' => TRUE,
  );

  // this matches the check in biblio.import.export.inc's biblio_save_node().
  if (user_access('administer nodes')) {
    module_load_include('inc', 'biblio', 'biblio.import.export');
    if (function_exists('_biblio_admin_build_user_select')) {
      // if statement redundant; no need to risk a parse error for this gravy
      // @TODO honestly an autocomplete would be a better interface
      $form['userid'] = _biblio_admin_build_user_select($user->uid);
      $form['userid']['#title'] = t('Set user credited as posting biblios:');
    } else {
      watchdog('entrez', 'Expected function _biblio_admin_build_user_select not available from biblio module in file biblio.import.export.inc.', WATCHDOG_ERROR);
    }
  }

  if (FALSE && module_exists('taxonomy')) {
    module_load_include('inc', 'entrez', 'entrez.taxonomy');
    $form['import_taxonomy'] = entrez_form_taxonomy();
  }

  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Import from PubMed'),
  );

  return $form;
}

/**
 * Implementation of validation for entrez_search_import_form.
 */
function entrez_search_import_form_validate($form, &$form_state) {
  $search_query = $form_state['values']['search_query'];
  $min_date = $form_state['values']['min_date'];
  $max_date = $form_state['values']['max_date'];

  if (empty($search_query)) {
    form_set_error('entrez_pubmed_search',
      t('Please enter at least one Search term.'));
  }
  elseif ($min_date >= $max_date) {
    form_set_error('entrez_pubmed_search',
      t('Minimum date must be earlier than Maximum date.'));
  }
}

/**
 * Implementation of validation for entrez_pubmed_id_import_form.
 */
function entrez_pubmed_id_import_form_validate($form, &$form_state) {
  $pmids = entrez_au_rationalize_separators($form_state['values']['entrez_pubmed_id']);

  if (empty($pmids)) {
    form_set_error('entrez_pubmed_id', t('Please enter at least one PubMed ID.'));
  }
  else {
    $errors = array();

    for ($i = 0; $i < count($pmids); $i++) {
      if (!ctype_digit($pmids[$i]) || $pmids[$i] <= 0) {
        // "@" sends the input through check_plain to prevent security breaches
        $errors[] = t('A value entered, %pmid, is not a positive integer.', array('%pmid' => $pmids[$i]));
      }
    }

    if (!empty($errors)) {
      // Report how many and which entered pubmed IDs were rejected
      form_set_error('entrez_pubmed_id', implode(' ', $errors) . ' ' . t('Please ensure you input valid PubMed IDs.'));
    }
    else {
      // add the cleaned-up pubmed IDs to the form values
      // these are passed to submit by reference
      $form_state['values']['pmids'] = $pmids;
    }
  }
}

/**
 * Implementation of submit for the entrez_search_import_form.
 *
 * @param $form
 * @param $form_state
 */
function entrez_search_import_form_submit($form, &$form_state) {
  $search_query = $form_state['values']['search_query'];
  $min_date = $form_state['values']['min_date'];
  $max_date = $form_state['values']['max_date'];

  entrez_set_query(array(
    'query' => $search_query,
    'min_date' => $min_date,
    'max_date' => $max_date,
  ));

  if (isset($form_state['values']['userid'])) {
    $uid = $form_state['values']['userid'];
  }
  else {
    global $user;
    $uid = $user->uid;
  }

  // if we allow creation of free tagging terms, this will become complicated.
  // we will have to know the vocabulary and create the terms first
  $term_ids = array();
  $taxonomy = $form_state['values']['taxonomy']; // works because form tree true

  if (isset($taxonomy) && is_array($taxonomy)) {
    foreach ($taxonomy as $vid => $vocabulary) {
      $vocab_term_ids = array_keys($vocabulary);  // this changes for free tags
      $term_ids = array_merge($term_ids, $vocab_term_ids);
    }
  }

  $node_info = array(
    'uid' => $uid,
    'taxonomy' => $term_ids,
    'status' => 0, // set bulk imported to unpublished by default
  );

  $entrez = entrez_create_client();
  $entrez->setReturnMax(25);
  $entrez->setTerm($form_state['values']['search_query']);
  $entrez->setDateRange($min_date, $max_date);

  try {
    $entrez->search();
    // Disable indexing during mass import. Run afterwards.
    variable_set('biblio_index', 0);

    $devel = entrez_mode_check('development');
    $override = FALSE;

    if (isset($form_state['values']['override_import_max']) &&
        $form_state['values']['override_import_max'] == TRUE) {
      $override = TRUE;
    }

    if ($entrez->count() > ENTREZ_IMPORT_MAX && !$devel && !$override) {
      drupal_set_message(t('The search query was saved but the import was not performed because the result set contained more than @max articles (it contained @count).  This could adversely affect the experience of people using the site during the import.  Please refine your search (including start and stop dates) or arrange for the query to be done at a time or location that will not interfere with normal usage of the site.', array('@max' => ENTREZ_IMPORT_MAX, '@count' => $entrez->count())), 'warning');
    }
    elseif ($entrez->count() > $entrez->getReturnMax()) { // go to batch mode
      $destination = $_GET['q'];
      $batch_op = array(
        'title' => t('Importing @count Pubmed articles', array('@count' => $entrez->count())),
        'operations' => array(
          array(
            'entrez_import_batch',
            array($entrez, $node_info, $entrez->getReturnMax()),
          ),
        ),
        'progressive' => TRUE,
        'finished' => 'entrez_import_batch_finished',
        'init_message' => t('Processing bibliography imports from PubMed.'),
        'progress_message' => t('Importing from PubMed and saving bibliographies.'),
        'file' => 'entrez.batch.inc',
      );
      batch_set($batch_op);
      batch_process($destination);
    }
    elseif ($entrez->count() > 0) {
      $imported = entrez_import_pubmed($entrez->fetchResult(), $node_info);
      $message = format_plural(count($imported), 'One bibliography saved.',
        '@count bibliographies saved.');
      $not_imported = $entrez->count() - count($imported);
      if ($not_imported > 0) {
        $message .= format_plural($not_imported,
          'One bibliography has not changed since last import.',
          '@count bibliographies have not changed since last import.');
      }

      drupal_set_message($message);
    }
    else {
      drupal_set_message(t('Sorry, your search returned no results.'));
    }
  }
  catch (Exception $exception) {
    drupal_set_message(t('An error occurred when requesting data from ' .
      'PubMed. Try again later.'));
  }
}

/**
 * Imports Pubmed documents matching the IDs entered by the user.
 *
 * @param $form
 * @param $form_state
 */
function entrez_pubmed_id_import_form_submit($form, &$form_state) {
  if (isset($form_state['values']['userid'])) {
    $uid = $form_state['values']['userid'];
  }
  else {
    global $user;
    $uid = $user->uid;
  }

  $nodes = entrez_pubmed_id_import_all($form_state['values']['pmids'], $uid);
  foreach($nodes as $node) {
    if (!$node->pubmed_updated) {
      $node->uid = $form_state['values']['userid'];
      drupal_set_message(t('Successfully imported bibliographic citation !link.',
        array('!link' => l($node->title, 'node/' . $node->nid))));
    } else {
      drupal_set_message(t('Bibliographic citation !link is already in the database.',
        array('!id' => $pmid, '!link' =>l($node->title, 'node/' . $node->nid))));
    }
  }
}
