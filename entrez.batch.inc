<?php
/**
 * @file
 * Batch operations for the entrez module.
 */

/**
 * Batch callback operation: Import PubMed articles into Biblio.
 *
 * @param $entrez
 *   A configured entrez client object.
 * @param $node_info
 *   Array of node information to act as default for imported bibliographies.
 * @param $size
 *   Number of articles to import in each operation.
 * @param $context
 *   Batch context containing state information.
 */
function entrez_import_batch($entrez, $node_info, $size, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $entrez->count();
    $context['results']['imported'] = 0;
    $context['results']['retrieved'] = 0;
  }

  $i = $context['sandbox']['progress'];

  // Sometimes the NCBI server responds with errors. Retry in the next batch.
  try {
    $imported = entrez_import_pubmed($entrez->fetchResult($i), $node_info);
    $i = min($i + $entrez->getReturnMax(), $entrez->count());
    $context['results']['imported'] += count($imported);
    $context['results']['retrieved'] = $i;
  }
  catch (Exception $exception) {
    watchdog('entrez', $exception->getMessage(), array(), WATCHDOG_ERROR);
  }

  $context['sandbox']['progress'] = $i;
  // Multistep processing : report progress.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}


/**
 * Prints out a message.
 *
 * @param $success
 * @param $results
 * @param $operations
 */
function entrez_import_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural($results['imported'], 'One bibliography saved.',
      '@count bibliographies saved.');
    $not_imported = $results['retrieved'] - $results['imported'];
    if ($not_imported > 0) {
      $message .= format_plural($not_imported,
        'One bibliography has not changed since last import.',
        '@count bibliographies have not changed since last import.');
    }
  }
  else {
    $message = t('Import finished with an error.');
  }

  drupal_set_message($message);
}
