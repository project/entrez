
-- SUMMARY --

Drupal Entrez Database Import module allows importing PubMed articles by ID 
and by Search term into the biblio database. Moderated mass retrieval per 
Batch API and cronjob as well as automatic updates in cron makes it a valuable 
addition to the biblio module.

Built mostly by Agaric Design Collective on behalf of the Science Collaboration 
Framework.

For a full description visit the project page:
  http://drupal.org/project/entrez
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/entrez

-- REQUIREMENTS --

* Biblio, http://drupal.org/project/biblio
* Date API, http://drupal.org/project/date

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- USAGE --

Manage the module at Administer >> Content Management >> Entrez.
* Settings: Enable and suspend periodic tasks for importing and updating your 
  PubMed items in the biblio database.
* Moderate biblios (only available with module entrezview)
* Define a search in Import by search term or Import by Pubmed ID

-- CONTACT --

Current maintainers:
* Stefan Freudenberg (stefan-agaric) <stefan@agaric.com>
* Benjamin Melancon (benjamin-agaric) <ben@agaric.com>

Development of this project has been funded by:
* Initiative in Innovative Computing at Harvard, http://iic.harvard.edu
