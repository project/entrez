<?php
/**
 * @file
 * Adapts drupal_http_request to the EntrezRequest interface.
 */

class DrupalHttpRequestWrapper implements EntrezRequest {
  public function get($url) {
    $result = drupal_http_request($url);

    if ($result->code != 200) {
      throw new Exception($result->error, $result->code);
    }

    return $result->data;
  }
}
