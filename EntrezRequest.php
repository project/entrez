<?php
/**
 * @file
 * Defines interface for HTTP requests. This allows applications to
 * make their own implementations available to the EntrezClient.
 */

interface EntrezRequest
{

  /**
   * Issues an HTTP GET request to the given resource.
   *
   * @param $url
   * @return string
   * The content body of the http get request if successful.
   * @throws Exception
   * An exception containing the http error code.
   */
  public function get($url);

}
