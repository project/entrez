<?php
/**
 * Short description for file
 *
 * PHP version 5
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @package     entrez
 * @author      Stefan Freudenberg <stefan@agaric.com>
 * @copyright   2009 Stefan Freudenberg
 * @license     http://www.gnu.org/licenses/gpl.html
 */

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . '/..');

require_once 'PHPUnit/Framework/Test.php';
require_once 'EntrezClient.php';
require_once 'EntrezRequest.php';
require_once 'TestRequest.php';

class EntrezClientTestCase extends PHPUnit_Framework_TestCase {
  protected $object;

  protected function setUp()
  {
    $this->object = new EntrezClient();
    $request = new TestRequest();
    $request->test_file = 'pubmed_article.xml';
    $this->object->setRequest($request);
  }

  public function testSetAndGetDateRange() {
    $this->setExpectedException('Exception',
      'First argument must be an earlier date.');
    $this->object->setDateRange('2009/09/09', '2009/09/09');
    $this->assertNull($this->object->getDateRange());

    $this->object->setDateRange('1949/09/09', '1969/09/10');
    $this->assertEquals('1949/09/09', $this->object->getMinDate());
    $this->assertEquals('1969/09/10', $this->object->getMaxDate());
  }

  public function testFetch()
  {
    $this->assertXmlStringEqualsXmlString(
      $this->object->fetch(19479730)->asXML(),
      file_get_contents('pubmed_article.xml'));
  }

}
