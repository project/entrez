<?php
/**
 * @file
 * Test implementation of EntrezRequest
 */

class TestRequest implements EntrezRequest
{
  public $test_file;

  public function get($url) {
    $url_components = parse_url($url);

    if ($url_components['host'] != 'eutils.ncbi.nlm.nih.gov') {
      throw new Exception("Wrong hostname: $url_components[host]");
    }

    if (basename($url_components['path']) != 'efetch.fcgi') {
      throw new Exception("Unsupported path: $url_components[path]");
    }

    parse_str($url_components['query'], $query_params);

    if (!isset($query_params['id'])) {
      throw new Exception("Missing parameter: id");
    }

    return file_get_contents($this->test_file);
  }

}
